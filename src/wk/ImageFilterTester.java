package wk;

import java.applet.Applet;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageFilterTester extends Applet {
	ImageFilterControl filterControl;

	public void init() {
		BufferedImage image = null;
		try {
			image = ImageIO.read(this.getClass().getResource("image01.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		setLayout(gbl);

		// add button
		Button b = new Button("Apply Filter");
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbl.setConstraints(b, gbc);
		add(b);

		//add image canvas
		filterControl = new ImageFilterControl(image);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 2.0;
		gbc.weighty = 2.0;
		gbl.setConstraints(filterControl, gbc);
		add(filterControl);
		
		validate();
	}
	public boolean handleEvent(Event e){
		if(e.id==Event.WINDOW_DESTROY)
			System.exit(0);
		return super.handleEvent(e);
	}
	public boolean action(Event evt, Object arg){
		filterControl.applyFilter();
		return true;
	}

}
