package wk;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import wk.filters.BlurFilter;
import wk.filters.CompoundFilter;
import wk.filters.LomoFilter;
import wk.filters.OilPaintFilter;
import wk.filters.OldFilter;

public class FilterGenerator {
	public static void main(String[] args) {
		BufferedImage src, dest;

		File directory = new File(
				"D:\\Education\\University Education\\YEAR 2.1 (AUG 2012)\\eclipse\\CS3246 PROJECT\\images\\swissmountains");
		String[] fl = directory.list();

		for (String s : fl) {
			try {
				System.out.println(directory.getCanonicalPath()+"\\"+s);
				src = ImageIO.read(new File(directory.getCanonicalPath()+"\\"+s));
				BufferedImageOp op0 = new BlurFilter();
				BufferedImageOp op1 = new LomoFilter();
				BufferedImageOp op2 = new OldFilter();
				BufferedImageOp op3 = new OilPaintFilter(3);
				
				BufferedImageOp op = new CompoundFilter(op1, op2);
				
				dest = op.filter(src, null);

				File outputfile = new File(directory.getCanonicalPath()+"\\Compound\\"+"Compound_"+s);

				ImageIO.write(dest, "jpg", outputfile);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}
