package wk;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import wk.filters.BlurFilter;
import wk.filters.CompoundFilter;
import wk.filters.LomoFilter;
import wk.filters.OilPaintFilter;
import wk.filters.OldFilter;

public class ImageFilterControl extends Canvas {
	BufferedImage src, dest;

	public ImageFilterControl(BufferedImage image) {
		src = image;
		dest = image;
	}

	public void applyFilter() {

		 BufferedImageOp op4 = new BlurFilter();
		BufferedImageOp op3 = new OilPaintFilter(3);
		BufferedImageOp op1 = new LomoFilter();
		BufferedImageOp op2 = new OldFilter();
		BufferedImageOp op = new CompoundFilter(op1, op2);
		dest = op4.filter(src, null);

		File outputfile = new File("saved.jpg");
		try {
			ImageIO.write(dest, "jpg", outputfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		repaint();

	}

	public void paint(Graphics g) {
		Dimension d = size();

		int x = (d.width - src.getWidth(this)) / 2;
		int y = (d.height - src.getHeight(this)) / 2;

		g.drawImage(dest, x, y, this);
	}

}
